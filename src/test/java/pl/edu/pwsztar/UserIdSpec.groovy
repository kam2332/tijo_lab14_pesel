package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class UserIdSpec extends Specification {

    @Unroll
    def "should check correct size"() {
        given: "initial data"
            def user = new UserId(id)
        expect:
            user.isCorrectSize()
        where:
            id << [
                "48081937855",
                "72051679638",
                "53082356745",
                "98021999847"
            ]
    }

    @Unroll
    def "should check incorrect size"() {
        given: "initial data"
            def user = new UserId(id)
        expect:
            !user.isCorrectSize()
        where:
            id << [
                "4808341937855",
                "",
                "53745",
                null
            ]
    }

    @Unroll
    def "should check sex"() {
        given: "initial data"
        def user = new UserId(id)
        expect:
        user.getSex() == sex
        where:
        id | sex
        "48041371169" | Optional.of(UserIdChecker.Sex.WOMAN)
        "93111119189" | Optional.of(UserIdChecker.Sex.WOMAN)
        "69012542777" | Optional.of(UserIdChecker.Sex.MAN)
        "70070793733" | Optional.of(UserIdChecker.Sex.MAN)
    }

    @Unroll
    def "should check date"() {
        given: "initial data"
        def user = new UserId(id)
        expect:
        user.getDate() == date
        where:
        id | date
        "48841371163" | Optional.of("13-04-1848")
        "93111119189" | Optional.of("11-11-1993")
        "69212542773" | Optional.of("25-01-2069")
        "70470793643" | Optional.of("07-07-2170")
        "23711447939" | Optional.of("14-11-2223")
    }

    @Unroll
    def "should check correct id"() {
        given: "initial data"
        def user = new UserId(id)
        expect:
        user.isCorrect()
        where:
        id << [
                "48081937855",
                "72051679638",
                "53082356745",
                "98021999847"
        ]
    }

    @Unroll
    def "should check incorrect id"() {
        given: "initial data"
        def user = new UserId(id)
        expect:
        !user.isCorrect()
        where:
        id << [
                "46a819678v8",
                "720516738",
                "a",
                null
        ]
    }
}
