package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

final class UserId implements UserIdChecker {

    private final String id;    // NR. PESEL

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
        if (id == null || id.equals("")) {
            return false;
        }
        return id.length() == 11;
    }

    @Override
    public Optional<Sex> getSex() {
        if (!isCorrect()) {
            return Optional.empty();
        }
        final Optional<Sex> sex = Optional.of(
                Integer.parseInt(
                        String.valueOf(id.charAt(9))) % 2 == 0
                        ? Sex.WOMAN
                        : Sex.MAN);
        return sex;
    }

    @Override
    public boolean isCorrect() {
        if(!isCorrectSize()) {
            return false;
        }
        List<Integer> digits;
        try {
            digits = Arrays.stream(id.split("")).map(Integer::parseInt).collect(Collectors.toList());
        } catch (NumberFormatException exception) {
            return false;
        }
        List<Integer> weights = Arrays.asList(1,3,7,9,1,3,7,9,1,3,1);
        int sum = IntStream.range(0, digits.size())
                .map(index -> digits.get(index) * weights.get(index)).sum();
        return sum % 10 == 0;
    }

    @Override
    public Optional<String> getDate() {
        if (!isCorrect()) {
            return Optional.empty();
        }
        int thirdDigit = Integer.parseInt(id.substring(2,3));
        int day = Integer.parseInt(id.substring(4,6));
        int month = (thirdDigit%2)*10 + Integer.parseInt(id.substring(3,4));
        int year = 1900 + Integer.parseInt(id.substring(0,2));
        if (thirdDigit >= 2 && thirdDigit < 8) {
            year += Integer.parseInt(String.valueOf((int)Math.floor(thirdDigit/2.0)))*100;
        } else if (thirdDigit >= 8) {
            year -= 100;
        }
        System.out.println((day < 10 ? "0" : "") + day + "_"+ (month < 10 ? "0" : "") + month + "_"+ year);
        return Optional.of((day < 10 ? "0" : "") + day + "-"+ (month < 10 ? "0" : "") + month + "-"+ year);
    }
}
